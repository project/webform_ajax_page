/*jshint strict:true, browser:true, curly:true, eqeqeq:true, expr:true, forin:true, latedef:true, newcap:true, noarg:true, trailing: true, undef:true, unused:true */
/*global Drupal: true, jQuery: true*/
(function ($) {
  "use strict";
  Drupal.behaviors.webformAjaxPage = {
    attach: function () {
      var amount = 0;
      var alltabs = [];
      
      function hideOrShow($tabs, wrapperid, size, hide_submit) {
        var $input;
        if ($tabs.tabs('option', 'selected') !== size) {
          $('#' + wrapperid + ' .form-actions input').each(function () {
            $input = $(this);
            if (hide_submit && !$input.hasClass('cancel') && !$input.is('#edit-draft')) {
              $input.hide();
            }
          });
        }
        else{
          $('#' + wrapperid + ' .form-actions input').each(function(){
            $input = $(this);
            if (!$input.hasClass('cancel') && !$input.is('#edit-draft')) {
              $input.show();
            }
          });
        }
      }
      
      //generate the tabs
      for( var key in Drupal.settings.WebFormAjaxPage.forms) {
        if (Drupal.settings.WebFormAjaxPage.forms.hasOwnProperty(key)) {
          var form = Drupal.settings.WebFormAjaxPage.forms[key];
          var wrapper = form.wrapper;
          var id = form.id;
          var hide_submit = parseInt(form.hide_submit, 10);
          alltabs[id] = $("#" + wrapper).tabs({idPrefix: 'ui-tabs-' + amount});

          //get the amount of tabs
          var totalSize = $("#" + wrapper + " .webform-component-ajax-page").size() - 1;
          //hide submit button until last ajax page is reached
          hideOrShow(alltabs[id], id, totalSize, hide_submit);
          amount += 1;

          //add the next and previous links
          $("#" + wrapper + " .webform-component-ajax-page").each(function (i) {
            var $self = $(this);
            var next, prev;
            var html = '<div class="webform-ajax-page-nav">';

            if (i !== 0) {
              prev = i - 1;
              html += "<a href='#' class='prev-tab mover' rel='" + prev + "'>" + Drupal.settings.WebFormAjaxPage.labels[$self.attr('id')].previous + "</a>";
            }

            if (i !== totalSize) {
              next = i + 1;

              html += "<a href='#' class='next-tab mover' rel='" + next + "'>" + Drupal.settings.WebFormAjaxPage.labels[$self.attr('id')].next + "</a>";
            }

            html += '</div>';
            $self.append(html);
          });

          //next and previous links handlers
          $("#" + wrapper + ' .next-tab, #' + wrapper + ' .prev-tab').click(function (e) {
            var $self = $(this);
            var formid = $self.closest('form').attr('id');
            if ($self.hasClass('next-tab') && typeof (Drupal.settings.clientsideValidation) !== 'undefined') {
              if ($('#' + formid).valid()) {
                alltabs[formid].tabs('select', parseInt($self.attr("rel"), 10));
              }
            }
            else{
              alltabs[formid].tabs('select', parseInt($self.attr("rel"), 10));
            }
            hideOrShow(alltabs[formid], formid, totalSize, hide_submit);
            if ($self.hasClass('prev-tab') && typeof (Drupal.settings.clientsideValidation) !== 'undefined') {
              $('#' + formid).find('.error').removeClass('error');
              $('#' + formid).valid();
            }
            var x = $('#' + wrapper).offset().top - 100;
            $('html, body').animate({scrollTop: x}, 1000);
            e.preventDefault();
          });
          if (hide_submit) {
            $('#' + id + ' #edit-actions').appendTo($('#' + id + ' .webform-component-ajax-page .webform-ajax-page-nav'));
          }
        }
      }
    }
  };
})(jQuery);
